package command

import (
	"fmt"
	"strings"
)

type help struct {
	commands []string
}

func (h *help) run(args []string) commandResult {
	res := commandResult{}
	if len(args) < 1 {
		res.Text = fmt.Sprintf("available commands: %s", strings.Join(h.commands, ", "))
		return res
	}
	res.Text = "login command was here"
	return res
}
