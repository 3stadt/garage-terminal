package command

import (
	"errors"
	"fmt"
	"strings"
)

type command interface {
	run(args []string) commandResult
}

type commandResult struct {
	Text string
	Err  error
}

type commandRequest struct {
	command string
	args    []string
	result  chan commandResult
}

var (
	commands map[string]command
	hub      chan commandRequest
)

func init() {
	commands = make(map[string]command)
	commands["help"] = &help{commands: []string{"help", "nick"}}
	hub = make(chan commandRequest)
	go execHub()
}

func Exec(input []byte) []byte {
	tokens := strings.Split(strings.TrimSpace(string(input)), " ")
	req := commandRequest{
		command: tokens[0],
		args:    tokens[1:],
		result:  make(chan commandResult),
	}
	hub <- req
	res := <-req.result
	close(req.result)
	if res.Err != nil {
		return []byte(fmt.Sprintf("[ERROR] %s", res.Err))
	}
	return []byte(res.Text)
}

func execHub() {
	for {
		select {
		case c := <-hub:
			if command, ok := commands[c.command]; ok {
				c.result <- command.run(c.args)
				break
			}
			c.result <- commandResult{
				Text: "",
				Err:  errors.New("command not found"),
			}
		}
	}
}
