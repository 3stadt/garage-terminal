module gitlab.com/3stadt/garage-terminal

go 1.14

require (
	fyne.io/fyne v1.2.4
	github.com/gorilla/websocket v1.4.2
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
)
