package main

import (
	"fmt"
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/widget"
	"github.com/phayes/freeport"
	"github.com/skratchdot/open-golang/open"
	"log"
	"net/http"
	"os"
)

func main() {
	port, err := freeport.GetFreePort()
	if err != nil {
		log.Fatal(err)
	}
	dsn := fmt.Sprintf("localhost:%d", port)
	hub := newHub()
	go hub.run()
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	http.Handle("/", http.FileServer(http.Dir("./www")))
	go func() {
		err = http.ListenAndServe(dsn, nil)
		if err != nil {
			fmt.Printf("Error: %s\n", err)
			os.Exit(1)
		}
	}()
	a := app.New()
	w := a.NewWindow("Garage Terminal")
	w.Resize(fyne.Size{
		Width:  250,
		Height: 50,
	})
	w.SetContent(widget.NewVBox(
		widget.NewButton("Start", func() {
			err := open.Start(fmt.Sprintf("http://%s", dsn))
			if err != nil {
				fmt.Printf("Error: %s\n", err)
				os.Exit(1)
			}
		}),
		widget.NewButton("Quit", func() {
			a.Quit()
		}),
	))

	w.ShowAndRun()
}
